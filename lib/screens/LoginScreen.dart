// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:flutter_vectracom_test/Controller/productController.dart';
import 'package:flutter_vectracom_test/screens/maindashbard.dart';
import 'package:flutter_vectracom_test/services/navigation_service.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import '../utils/service_locator.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<LoginPage> {
  var navigationService = locator<NavigationService>();
  TextEditingController titleController = TextEditingController();
  TextEditingController bodydController = TextEditingController();
  bool isLoadingProgress = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
          padding: const EdgeInsets.all(10),
          child: ListView(
            children: <Widget>[
              Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.all(31),
                  child: const Text(
                    'Sign In',
                    style: TextStyle(
                        color: Color.fromRGBO(54, 192, 73, 1),
                        fontWeight: FontWeight.w500,
                        fontSize: 30),
                  )),
              Container(
                padding: const EdgeInsets.all(10),
                child: TextField(
                  controller: titleController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                child: TextField(
                  keyboardType: TextInputType.number,
                  obscureText: true,
                  controller: bodydController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                  height: 50,
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: const Color.fromRGBO(54, 192, 73, 1),
                    child: const Text('Login'),
                    onPressed: () async {
                      if (titleController.text == "" ||
                          bodydController.text == "") {
                        Fluttertoast.showToast(
                            msg: "please fill all fields",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.TOP,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.grey,
                            textColor: Colors.white,
                            fontSize: 16.0);
                      } else {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (ctx) => Maindahboard(
                              pageIndex: 1,
                            ),
                          ),
                        );
                      }
                    },
                  )),
            ],
          )),
    );
  }
}

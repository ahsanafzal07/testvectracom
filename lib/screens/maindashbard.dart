import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vectracom_test/Controller/productController.dart';
import 'package:flutter_vectracom_test/screens/home_screen.dart';

import 'package:flutter_vectracom_test/screens/settingscreen.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../services/navigation_service.dart';
import '../utils/service_locator.dart';
import 'LoginScreen.dart';

class Maindahboard extends StatefulWidget {
  // ignore: prefer_typing_uninitialized_variables
  final pageIndex;

  Maindahboard({this.pageIndex});

  @override
  _MaindahboardState createState() => _MaindahboardState();
}

class _MaindahboardState extends State<Maindahboard> {
  var navigationService = locator<NavigationService>();
  final ProductController prodcutController = Get.put(ProductController());

  var _bottomNavIndex = 0;

  final iconList = [
    "assets/images/Profile-Icon-(InActive).png",
    "assets/images/Business-Home-Icon.png",
    "assets/images/Setting.png",
  ];
  final nameList = [
    "Login",
    "Home",
    "Setting",
  ];

  void apiCall() async {
    await prodcutController.getAllData(context);
  }

  @override
  void initState() {
    apiCall();
    if (widget.pageIndex != null) _bottomNavIndex = widget.pageIndex;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // floatingActionButton: FloatingActionButton(
      //   backgroundColor: Theme.of(context).primaryColor,
      //   // child: Image.asset(
      //   //   "assets/images/Scan.png",
      //   //   scale: 3.5,
      //   // ),
      //   onPressed: () {
      //     Navigator.of(context).push(
      //       MaterialPageRoute(
      //         builder: (ctx) => LoginPage(),
      //       ),
      //     );
      //   },
      //   //params
      // ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      appBar: AppBar(
        centerTitle: true,
        title: _bottomNavIndex == 0
            ? const Text('Login Screen')
            : _bottomNavIndex == 1
                ? const Text('Home Screen')
                : const Text('Setting Screen'),
        backgroundColor: Theme.of(context).backgroundColor,
        elevation: 0,
        bottomOpacity: 0,
        automaticallyImplyLeading: false,
      ),
      bottomNavigationBar: AnimatedBottomNavigationBar.builder(
        tabBuilder: (int index, bool isActive) {
          final color = isActive ? Color.fromRGBO(54, 192, 73, 1) : Colors.grey;
          return Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                iconList[index].toString(),
                scale: 4,
                color: color,
              ),
              const SizedBox(height: 5),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Text(
                  nameList[index],
                  maxLines: 1,
                  style: TextStyle(
                      fontSize: 10, color: color, fontWeight: FontWeight.bold),
                ),
              )
            ],
          );
        },

        //  icons: iconList,
        activeIndex: _bottomNavIndex,
        gapLocation: GapLocation.none,
        notchSmoothness: NotchSmoothness.defaultEdge,

        onTap: (index) {
          // if (nameList[index] == 'Upload Receipt') {
          setState(() => _bottomNavIndex = index);
          // }
          // else {
          //   setState(() => _bottomNavIndex = index);
          // }
        },
        itemCount: 3,
        //other params
      ),
      body: IndexedStack(
        index: _bottomNavIndex,
        children: <Widget>[
          LoginPage(),
          HomeScreen(),
          SettingScreen(),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get_storage/get_storage.dart';

import '../main.dart';

class SettingScreen extends StatefulWidget {
  @override
  State<SettingScreen> createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  bool isSwitched = false;
  var them;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          ListTile(
              title: Text(them ?? "Light Theme"),
              trailing: Switch(
                  value: isSwitched,
                  onChanged: (value) {
                    if (isSwitched == false) {
                      setState(() {
                        isSwitched = value;
                        Get.changeTheme(ThemeData.dark());
                        them = "Dark Them";
                      });
                    } else {
                      setState(() {
                        isSwitched = value;
                        Get.changeTheme(ThemeData.light());
                        them = "Light Them";
                      });
                    }
                  }))
        ],
      ),
    );
  }
}

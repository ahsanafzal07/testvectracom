import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

import '../../services/navigation_service.dart';
import '../../utils/service_locator.dart';
import '../Controller/productController.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var navigationService = locator<NavigationService>();
  final ProductController prodcutController = Get.put(ProductController());

  bool isloading = false;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: const Size(360, 690),
    );
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
          color: Color.fromRGBO(253, 254, 255, 1),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        ),
        child: FutureBuilder(
            future: prodcutController.getAllData(context),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                if (snapshot.error != null) {
                  return const Center(
                    child: Text('An error occured'),
                  );
                } else {
                  return Obx(() {
                    return ListView.builder(
                        itemCount: prodcutController.getAllApiResponse.length,
                        itemBuilder: (ctx, i) {
                          return Container(
                              padding: EdgeInsets.only(
                                  left: 7.h, right: 7.h, top: 21),
                              decoration: const BoxDecoration(boxShadow: [
                                BoxShadow(
                                    color: Color.fromRGBO(232, 237, 241, 1),
                                    blurRadius: 2,
                                    spreadRadius: 3,
                                    offset: Offset(0, 7))
                              ]),
                              child: Card(
                                  elevation: 4.0,
                                  child: Column(
                                    children: [
                                      ListTile(
                                        title: Text(prodcutController
                                            .getAllApiResponse[i].name
                                            .toString()),
                                        subtitle: Text(prodcutController
                                            .getAllApiResponse[i].habitat
                                            .toString()),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.all(16.0),
                                        alignment: Alignment.centerLeft,
                                        child: Text(prodcutController
                                            .getAllApiResponse[i].diet
                                            .toString()),
                                      ),
                                      SizedBox(
                                        height: 200.0,
                                        child: Ink.image(
                                          image: NetworkImage(prodcutController
                                              .getAllApiResponse[i].imageLink
                                              .toString()),
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ],
                                  )));
                        });
                  });
                }
              }
            }),
      ),
    );
  }
}

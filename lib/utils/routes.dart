import 'package:flutter/material.dart';

import '../screens/settingscreen.dart';
import '../screens/maindashbard.dart';
import '../screens/LoginScreen.dart';

const homeScreenRoute = '/home-screen';
const loginScreenRoute = '/login-screen';
const settingScreenRoute = '/clipPath-screen';
const MaindDashboardRoute = '/maindashboard';

Route<dynamic> onGenerateRoute(RouteSettings settings) {
  switch (settings.name) {
    case loginScreenRoute:
      return MaterialPageRoute(builder: (BuildContext context) => LoginPage());

    case settingScreenRoute:
      return MaterialPageRoute(
          builder: (BuildContext context) => SettingScreen());
    case homeScreenRoute:
      return MaterialPageRoute(
          builder: (BuildContext context) => Maindahboard(
                pageIndex: 0,
              ));

    default:
      return MaterialPageRoute(
          builder: (BuildContext context) => Maindahboard(
                pageIndex: 0,
              ));
  }
}

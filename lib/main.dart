// @dart=2.9

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_vectracom_test/utils/service_locator.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get_storage/get_storage.dart';

import 'utils/routes.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  setupLocator();
  runApp(
    const MyApp(),
  );
}
//

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Cincy Rewards',
      color: Theme.of(context).backgroundColor,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        textSelectionTheme: const TextSelectionThemeData(
          cursorColor: Color.fromRGBO(54, 188, 72, 1),
        ),
        backgroundColor: const Color.fromRGBO(54, 188, 72, 1),
        // ignore: deprecated_member_use
        primaryColor: const Color.fromRGBO(54, 188, 72, 1),
        visualDensity: VisualDensity.adaptivePlatformDensity,
        // Define the default font family.
        fontFamily: 'RedHatDisplay',
      ),
      onGenerateRoute: onGenerateRoute,
      initialRoute: MaindDashboardRoute,
    );
  }
}

import 'package:dio/dio.dart';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../ExceptonHandling/apiResponseHandle.dart';
import '../model/api_Model.dart';

class ProductController extends GetxController {
  var dio = Dio();
  getApiResponse(String baseUrl) async {
    var response = await dio.get(
      baseUrl,
    );
    print(response.data);
    return response;
  }

  // Api response without name and use of list of model //
  // List<ApiModel>? getAllApiResponse = [].obs as List<ApiModel>?;
  var isLoading = true.obs;
  var getAllApiResponse = <ApiModel>[].obs;
  Future<void> getAllData(BuildContext context) async {
    try {
      var response = await getApiResponse(
          "https://zoo-animal-api.herokuapp.com/animals/rand/10");
      var data = response.data;

      if (response.statusCode == 200) {
        for (var i in data) {
          getAllApiResponse.add(ApiModel.fromJson(i));
        }
      }

      print(data);
    } catch (erro) {
      dynamic error = erro;
      final errorMessage = DioExceptions.fromDioError(error);
      Fluttertoast.showToast(
        msg: errorMessage.toString(),
      );
    }
  }
}
